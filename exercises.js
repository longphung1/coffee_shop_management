const assert = require("assert");

/**
 * @typedef {Object} CoffeeEntry
 * @property {string} name Coffee item name
 * @property {number} price Coffee item price
 * @property {number} quantity Coffee item price
 * @property {string} currency
 */

/**
 * @param {Array<CoffeeEntry>} a
 * @returns {Array<CoffeeEntry>}
 */
const increasePriceBy10000 = (a) => {
  console.log("initial array: ", a);
  console.log("\n\n");
  // Processing array
  const newArray = a.map((entry) => {
    const newCoffeeEntry = { ...entry };
    newCoffeeEntry.price += 10000;

    console.log(">>>> Inside map");
    console.log("entry: ", entry);
    console.log("newCoffeeEntry: ", newCoffeeEntry);
    console.log("<<<<<<");

    return newCoffeeEntry;
  });

  console.log("\n\n");
  console.log("new array: ", newArray);

  return newArray;
};

/**
 * @param {Array<CoffeeEntry>} a
 * @param {Array<number>} timeToMake
 * @returns {Array<{ timeToMake: number } & CoffeeEntry>}
 */
const addTimeToMake = (a, timeToMake) => {
  // TODO: add timeToMake property to each item in a array, taken from the same index in timeToMake array. Return
  //  the new array
  return [];
};

/**
 * @param {Array<CoffeeEntry>} a
 * @returns {Array<CoffeeEntry>}
 */
const filterNoQuantityCoffee = (a) => {
  // TODO: filter out coffee list entries with 0 quantities
  return [];
};

/**
 * @param {Array<CoffeeEntry>} a
 * @returns {Array<CoffeeEntry>}
 */
const filterUnprofitableAndNoQuantityCofee = (a) => {
  // TODO: filter out coffee entries that cost lower than 10000 and 0 quantites left
  return [];
};

/**
 * @param {Array<CoffeeEntry>} a
 * returns number
 */
const getTotalAmountOfItems = (a) => {
  // TODO: return the total amount of coffee items by getting sum of all quantity
  return 0;
};

/**
 * Test array code
 */
const exercise1 = () => {
  /**
   * @type {Array<CoffeeEntry>}
   */
  const a = [
    {
      name: "Cà phê đá",
      price: 10000,
      currency: "VND",
      quantity: 10,
    },
    {
      name: "Cà phê sữa",
      price: 10000,
      currency: "VND",
      quantity: 0,
    },
    {
      name: "Bạc sỉu",
      price: 20000,
      currency: "VND",
      quantity: 15,
    },
  ];

  const timeToMake = [10, 15];
  const priceBy10000 = increasePriceBy10000(a);
  const addTimeToMakeArray = addTimeToMake(a, timeToMake);
  const filter0QuantityArray = filterNoQuantityCoffee(a);
  const filterUnprofitableArray = filterUnprofitableAndNoQuantityCofee(a);
  const totalQuantity = getTotalAmountOfItems(a);

  assert(priceBy10000.length, "priceBy10000 array must not be empty");
  assert(addTimeToMakeArray.length, "addTimeToMakeArray array must not be empty");
  assert(filter0QuantityArray.length, "filter0QuantityArray array must not be empty");
  assert(filterUnprofitableArray.length, "addTimeToMakeArray array must not be empty");

  priceBy10000.forEach((item, index) => {
    assert(
      item.price === a[index].price + 10000,
      `Expected: item price to be: ${a[index].price + 10000}. Got: ${item.price}`
    );
  });
  addTimeToMakeArray.forEach((item, index) => {
    assert(
      item.timeToMake === timeToMake[index],
      `Expected: item timeToMake to be: ${timeToMake[index]}. Got: ${item.timeToMake}`
    );
  });
  filter0QuantityArray.forEach((item) => {
    assert(item.quantity > 0, `Expected: item.quantity to be more than 0. Got: ${item.quantity}`);
  });
  filter0QuantityArray.forEach((item) => {
    assert(
      item.quantity > 0 && item.price > 10000,
      `Expected: item.quantity to be more than 0 and item.price to be larger than 10000. Got: item.quantity ${item.quantity} and item.price ${item.price}`
    );
  });
  assert(totalQuantity === 25, `Total quantity is incorrect. Expected: ${25}. Got: ${totalQuantity}`);
};

exercise1();
console.log("Ran without any error. 👍");
