import React from "react";
import CoffeeListItem from "./CoffeeListItem";

const sampleData = [
  {
    name: "Cà phê đá",
    price: 10000,
    currency: "VND",
  },
  {
    name: "Cà phê sữa",
    price: 10000,
    currency: "VND",
  },
];

const CoffeeList = () => {
  const coffeeItems = sampleData;
  /* =========
     Render
     ========= */
  return coffeeItems.map((item) => <CoffeeListItem key={item.name} {...item} />);
};

export default CoffeeList;
